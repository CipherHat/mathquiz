package uom.mathquiz.controller;

import uom.mathquiz.external.CountdownTimer;
import uom.mathquiz.external.MathClass;
import uom.mathquiz.model.answerstate.GameStrategy;
import uom.mathquiz.model.answerstate.GameStrategyFactory;
import uom.mathquiz.model.expressionvisitor.ExpressionGenerator;
import uom.mathquiz.model.game.GameStatus;
import uom.mathquiz.model.game.GameStatusCaretaker;
import uom.mathquiz.model.gamestate.GameStart;
import uom.mathquiz.model.gamestate.GameState;
import uom.mathquiz.view.MainFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by CipherHat on 3/1/14.
 */
public class GameController implements Observer, ActionListener {
    private ExpressionGenerator generator;
    private MathClass math;
    private GameState gameState;
    private MainFrame frame;
    private GameStatus gameStatus;
    private GameStatusCaretaker caretaker;
    private CountdownTimer countdownTimer;

    private boolean bool1, bool2, bool3, bool4, bool5, bool6, isCorrect;

    public GameController(MainFrame frame) {
        this.frame = frame;
        this.math = MathClass.getInstance();
        sleepQuiz();
    }

    /**
     * Game Over
     */
    public void sleepQuiz() {
        frame.enableButton1(false);
        frame.enableButton2(false);
        frame.enableButton3(false);
        frame.enableButton4(false);
        frame.enableButton5(false);
        frame.enableButton6(false);
        frame.enableButtonStart(true);
        gameStatus = new GameStatus();
        caretaker = new GameStatusCaretaker();
        generator = new ExpressionGenerator();
        gameState = new GameStart();

        getGameStatus().addObserver(this);
    }

    /**
     * starting point for each round
     */
    public void startQuiz() {
        frame.setQuestionLabelText(generator.getExpressionString().replaceAll("\\( ", "(").replaceAll(" \\)", ")"));
        frame.enableButtonStart(false);
        frame.enableButton1(true);
        frame.enableButton2(true);
        frame.enableButton3(true);
        frame.enableButton4(true);
        frame.enableButton5(true);
        frame.enableButton6(true);
    }

    /**
     * changing the state for each run, for now, it is a cycle of GameStart -> GameRunning -> GameStart.
     * May be expanded depending on the UI
     */
    public void run() {
        gameState.proceed(this);
    }


    //************************************************************************//
    //******************************** START *********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public ExpressionGenerator getGenerator() {
        return generator;
    }

    public void setGenerator(ExpressionGenerator generator) {
        this.generator = generator;
    }

    public CountdownTimer getCountdownTimer() {
        return countdownTimer;
    }

    public void setCountdownTimer(CountdownTimer countdownTimer) {
        this.countdownTimer = countdownTimer;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    //************************************************************************//
    //******************************** END ***********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    /**
     * After finished answering, the answer then will be evaluated using GameStrategy
     */
    public void processAnswer() {
        GameStrategyFactory factory = GameStrategyFactory.getInstance();
        GameStrategy gameStrategy = factory.getStrategy(isCorrect);
        gameStrategy.handleGameStatus(gameStatus, caretaker);

        if (caretaker.isFinalRound() && !isCorrect) {
            sleepQuiz();
            resetAnswer();
        } else {
            resetAnswer();
            if (countdownTimer.getTimeRemaining() > 0) {
                run();
            }
        }
    }

    /**
     * reset answer boolean to false
     */
    private void resetAnswer() {
        bool1 = false;
        bool2 = false;
        bool3 = false;
        bool4 = false;
        bool5 = false;
        bool6 = false;
        isCorrect = false;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof CountdownTimer) {
            int count = (Integer) arg;
            frame.setTimerLabelText("" + count);

            if (count <= 5 && count > 0) { //change UI countdown timer to Red when below 5 seconds
                frame.setTimerLabelForeground();
            } else if (count == 0) { //TIMES UP
                run();
                resetAnswer();
                sleepQuiz();
            }
        } else if (o instanceof GameStatus) {
            String text = arg.toString();
            if (text.contains("Level"))
                frame.setLevelLabelText("Level " + gameStatus.getLevel());
            else if (text.contains("Round"))
                frame.setRoundLabelText("Round " + gameStatus.getRounds());
            else if (text.contains("Win Count"))
                frame.setWinningLabelText("Win Count " + gameStatus.getWinCount());
            else if (text.contains("Lose Count"))
                frame.setLosingLabelText("Lose Count " + gameStatus.getLoseCount());
            else if (text.contains("Winning Streak"))
                frame.setWinningStreakLabelText("Winning Streak " + gameStatus.getWinningStreak());
            else if (text.contains("Highest Streak"))
                frame.setHighestWinningStreakLabelText("Highest Streak " + gameStatus.getHighestWinningStreak());
        }
    }

    /**
     * assigning answer for the expression to one of the button randomly
     *
     * @param result double. answer for the expression.
     */
    public void generateAnswer(Double result) {
        String[] checkFloat = ("" + result).split("\\.");
        if (Integer.parseInt(checkFloat[1]) == 0) {
            placeAnswer(Integer.parseInt(checkFloat[0]), math.genRandomNo(6, 1));
        } else if (Integer.parseInt(checkFloat[1]) > 0) {
            placeAnswer(result, math.genRandomNo(6, 1));
        }
    }

    /**
     * placing answer on the button as well as generating random number for other button.
     *
     * @param result double. answer for the expression.
     * @param place  int
     */
    private void placeAnswer(Double result, int place) {
        double maxNumber;
        // avoid generating answer range that is too large. if answer is outside the range of 1000,
        // just add another 1000;
        if (Math.abs(result) < 1000 && Math.abs(result) > 0)
            maxNumber = Math.abs(result) * 10;
        else
            maxNumber = Math.abs(result) + 1000;

        for (int i = 1; i <= 6; i++) {
            double possibleAnswer;
            boolean isTrue = false;
            if (i == place) {
                possibleAnswer = result;
                isTrue = true;
            } else
                possibleAnswer = math.genRandomNo(maxNumber, maxNumber - (maxNumber * 2));

            writeToButton(i, "" + possibleAnswer, isTrue);
        }
        System.out.println(bool1 + " " + bool2 + " " + bool3 + " " + bool4 + " " + bool5 + " " + bool6);
    }

    /**
     * placing answer on the button as well as generating random number for other button.
     *
     * @param answer int
     * @param place  int
     */
    private void placeAnswer(int answer, int place) {
        int maxNumber;
        // avoid generating answer range that is too large. if answer is outside the range of 1000,
        // just add another 1000;
        if (Math.abs(answer) < 1000 && Math.abs(answer) > 0)
            maxNumber = Math.abs(answer) * 10;
        else if (Math.abs(answer) == 0)
            maxNumber = 9;
        else
            maxNumber = Math.abs(answer) + 1000;

        for (int i = 1; i <= 6; i++) {
            int possibleAnswer;
            boolean isTrue = false;
            if (i == place) {
                possibleAnswer = answer;
                isTrue = true;
            } else
                possibleAnswer = math.genRandomNo(maxNumber, maxNumber - (maxNumber * 2));

            writeToButton(i, "" + possibleAnswer, isTrue);
        }
        System.out.println(bool1 + " " + bool2 + " " + bool3 + " " + bool4 + " " + bool5 + " " + bool6);
    }

    /**
     * with the provided possible answer, write to button
     *
     * @param i              int
     * @param possibleAnswer String
     * @param isTrue         boolean
     */
    private void writeToButton(int i, String possibleAnswer, boolean isTrue) {
        switch (i) {
            case 1:
                frame.setButton1Text(possibleAnswer);
                bool1 = isTrue;
                break;
            case 2:
                frame.setButton2Text(possibleAnswer);
                bool2 = isTrue;
                break;
            case 3:
                frame.setButton3Text(possibleAnswer);
                bool3 = isTrue;
                break;
            case 4:
                frame.setButton4Text(possibleAnswer);
                bool4 = isTrue;
                break;
            case 5:
                frame.setButton5Text(possibleAnswer);
                bool5 = isTrue;
                break;
            case 6:
                frame.setButton6Text(possibleAnswer);
                bool6 = isTrue;
                break;
            default:
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int buttonSelected = Integer.parseInt(((JButton) e.getSource()).getName());
        switch (buttonSelected) {
            case 1:
                isCorrect = bool1;
                break;
            case 2:
                isCorrect = bool2;
                break;
            case 3:
                isCorrect = bool3;
                break;
            case 4:
                isCorrect = bool4;
                break;
            case 5:
                isCorrect = bool5;
                break;
            case 6:
                isCorrect = bool6;
                break;
            case 7:
                gameStatus.initializeGame();
                break;
            default:
                System.out.println("wrong stuff");
                break;
        }
        run();
    }
}
