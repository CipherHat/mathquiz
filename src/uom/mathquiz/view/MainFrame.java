package uom.mathquiz.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by CipherHat on 3/1/14.
 */
public class MainFrame extends JFrame {
    private JPanel questionPanel;
    private JLabel questionLabel;
    private JLabel timerLabel;
    private JLabel levelLabel;
    private JLabel roundLabel;
    private JLabel winningLabel;
    private JLabel losingLabel;
    private JLabel winningStreakLabel;
    private JLabel highestWinningStreakLabel;

    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton buttonStart;

    public MainFrame() {
        createAndShowGUI();
    }

    public void addComponentsToPane(Container pane) {
        this.questionPanel = new JPanel();

        questionPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        //natural height, maximum width
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;

        c.gridwidth = 1;
        c.ipadx = 20;
        c.gridx = 0;
        c.gridy = 0;
        questionPanel.add(new JLabel(""), c);

        JLabel filler = new JLabel("Math Quiz");
        filler.setFont(new Font("Comic Sans MS", Font.BOLD, 40));
        filler.setHorizontalAlignment(SwingConstants.CENTER);
        c.gridwidth = 3;
        c.gridx = 1;
        c.gridy = 0;
        questionPanel.add(filler, c);

        c.gridwidth = 1;
        c.ipadx = 20;
        c.gridx = 4;
        c.gridy = 0;
        questionPanel.add(new JLabel(""), c);

        c.gridwidth = 5;
        c.ipady = 10;
        c.gridx = 0;
        c.gridy = 1;
        questionPanel.add(new JLabel(""), c);

        questionLabel = new JLabel("Some question");
        questionLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
        c.ipady = 20;
        c.ipadx = 1;
        c.gridwidth = 3;
        c.gridx = 1;
        c.gridy = 2;
        questionPanel.add(questionLabel, c);

        button1 = new JButton("1");
        button1.setName("1");
        c.gridwidth = 1;
        c.ipady = 40;
        c.ipadx = 50;
        c.gridx = 1;
        c.gridy = 3;
        questionPanel.add(button1, c);

        button2 = new JButton("2");
        button2.setName("2");
        c.gridx = 2;
        c.gridy = 3;
        questionPanel.add(button2, c);

        button3 = new JButton("3");
        button3.setName("3");
        c.gridx = 3;
        c.gridy = 3;
        questionPanel.add(button3, c);

        button4 = new JButton("4");
        button4.setName("4");
        c.gridx = 1;
        c.gridy = 4;
        questionPanel.add(button4, c);

        button5 = new JButton("5");
        button5.setName("5");
        c.gridx = 2;
        c.gridy = 4;
        questionPanel.add(button5, c);

        button6 = new JButton("6");
        button6.setName("6");
        c.gridx = 3;
        c.gridy = 4;
        questionPanel.add(button6, c);

        timerLabel = new JLabel("XX");
        timerLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 55));
        c.ipady = 0;      //make this component tall
        c.weightx = 0.0;
        c.gridheight = 3;
        c.gridx = 1;
        c.gridy = 5;
        questionPanel.add(timerLabel, c);

        filler = new JLabel("seconds left");
        filler.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
        c.gridx = 1;
        c.gridy = 8;
        c.gridheight = 1;
        questionPanel.add(filler, c);

        levelLabel = new JLabel("Level XX");
        levelLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
        c.ipady = 10;
        c.gridx = 2;
        c.gridy = 5;
        questionPanel.add(levelLabel, c);

        roundLabel = new JLabel("Round XX");
        roundLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
        c.gridx = 3;
        c.gridy = 5;
        questionPanel.add(roundLabel, c);

        winningLabel = new JLabel("Win Count XX");
        winningLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
        c.gridx = 2;
        c.gridy = 6;
        questionPanel.add(winningLabel, c);

        losingLabel = new JLabel("Lose Count XX");
        losingLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
        c.ipadx = 20;
        c.gridx = 3;
        c.gridy = 6;
        questionPanel.add(losingLabel, c);

        winningStreakLabel = new JLabel("Winning Streak XX");
        winningStreakLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
        c.gridx = 2;
        c.gridy = 7;
        questionPanel.add(winningStreakLabel, c);

        highestWinningStreakLabel = new JLabel("Highest Streak XX");
        highestWinningStreakLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
        c.gridx = 3;
        c.gridy = 7;
        questionPanel.add(highestWinningStreakLabel, c);

        buttonStart = new JButton("Start");
        buttonStart.setName("7");
        c.gridx = 3;
        c.gridy = 8;
        questionPanel.add(buttonStart, c);

        pane.add(questionPanel);
    }

    private void createAndShowGUI() {
        //Create and set up the window.
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }
        JFrame frame = new JFrame("Math Quiz");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public void setTimerLabelText(String timerLabelText) {
        this.timerLabel.setText(timerLabelText);
    }

    public void setTimerLabelForeground() {
        this.timerLabel.setForeground(Color.RED);
    }

    public void setQuestionLabelText(String questionLabelText) {
        questionLabel.setText(questionLabelText);
    }

    public void setHighestWinningStreakLabelText(String highestWinningStreakLabelText) {
        this.highestWinningStreakLabel.setText(highestWinningStreakLabelText);
    }

    public void setWinningStreakLabelText(String winningStreakLabelText) {
        this.winningStreakLabel.setText(winningStreakLabelText);
    }

    public void setLosingLabelText(String losingLabelText) {
        this.losingLabel.setText(losingLabelText);
    }

    public void setWinningLabelText(String winningLabelText) {
        this.winningLabel.setText(winningLabelText);
    }

    public void setRoundLabelText(String roundLabelText) {
        this.roundLabel.setText(roundLabelText);
    }

    public void setLevelLabelText(String levelLabelText) {
        this.levelLabel.setText(levelLabelText);
    }

    public void setButton1Text(String s) {
        this.button1.setText(s);
    }

    public void setButton2Text(String s) {
        this.button2.setText(s);
    }

    public void setButton3Text(String s) {
        this.button3.setText(s);
    }

    public void setButton4Text(String s) {
        this.button4.setText(s);
    }

    public void setButton5Text(String s) {
        this.button5.setText(s);
    }

    public void setButton6Text(String s) {
        this.button6.setText(s);
    }

    public void enableButtonStart(boolean enable) {
        buttonStart.setEnabled(enable);
    }

    public void enableButton1(boolean enable) {
        button1.setEnabled(enable);
    }

    public void enableButton2(boolean enable) {
        button2.setEnabled(enable);
    }

    public void enableButton3(boolean enable) {
        button3.setEnabled(enable);
    }

    public void enableButton4(boolean enable) {
        button4.setEnabled(enable);
    }

    public void enableButton5(boolean enable) {
        button5.setEnabled(enable);
    }

    public void enableButton6(boolean enable) {
        button6.setEnabled(enable);
    }

    public void addAllButtonListener(ActionListener e) {
        addButton1Listener(e);
        addButton2Listener(e);
        addButton3Listener(e);
        addButton4Listener(e);
        addButton5Listener(e);
        addButton6Listener(e);
        addButtonStartListener(e);
    }

    public void addButtonStartListener(ActionListener e) {
        buttonStart.addActionListener(e);
    }

    public void addButton1Listener(ActionListener e) {
        button1.addActionListener(e);
    }

    public void addButton2Listener(ActionListener e) {
        button2.addActionListener(e);
    }

    public void addButton3Listener(ActionListener e) {
        button3.addActionListener(e);
    }

    public void addButton4Listener(ActionListener e) {
        button4.addActionListener(e);
    }

    public void addButton5Listener(ActionListener e) {
        button5.addActionListener(e);
    }

    public void addButton6Listener(ActionListener e) {
        button6.addActionListener(e);
    }
}
