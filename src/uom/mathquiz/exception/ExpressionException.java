package uom.mathquiz.exception;

/**
 * Created by CipherHat on 2/15/14.
 */
public class ExpressionException extends Exception {
    public ExpressionException(String errorMsg) {
        super(errorMsg);
    }

    public ExpressionException(double num1, double num2, char parameter) {
        super(("Invalid numbers for arithmetic expression between " + num1 + " and " + num2 + " for operator " + parameter));
    }
}
