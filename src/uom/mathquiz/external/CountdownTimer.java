package uom.mathquiz.external;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CipherHat on 3/1/14.
 */
public class CountdownTimer extends Observable {
    private int interval;
    private Timer timer;

    public CountdownTimer(int interval) {
        timer = new Timer();
        this.interval = interval;
    }

    /**
     * start timer countdown by seconds.
     */
    public void startCountdown() {
        int delay = 1000;
        int period = 1000;
        timer.scheduleAtFixedRate(new RunTimer(), delay, period);
    }

    /**
     * stop timer countdown
     */
    public void stopCountdown() {
        timer.cancel();
    }

    private void setInterval() {
        if (interval == 1)
            timer.cancel();
        setChanged();
        notifyObservers(--interval);
    }

    /**
     * check the time remaining if stopCountdown() is invoked.
     * @return int
     */
    public int getTimeRemaining() {
        return interval;
    }

    private class RunTimer extends TimerTask {
        @Override
        public void run() {
            setInterval();
        }
    }
}
