package uom.mathquiz.external;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by CipherHat on 3/3/14.
 * <p/>
 * Singleton
 */
public class MathClass {
    private static MathClass instance;

    private Random random = new Random();

    private MathClass() {
    }

    public static MathClass getInstance() {
        if (instance == null) {
            instance = new MathClass();
        }
        return instance;
    }

    /**
     * generate Random number. may include negative numbers. max and min should be the exact number wanted.
     * e.g: wanted number ranging from 3,4,5,6 use max=6 and min=3 {genRandomNo(6,3)}
     *
     * @param max int
     * @param min int
     * @return int
     */
    public int genRandomNo(int max, int min) {
        return random.nextInt(max - min + 1) + min;
    }

    /**
     * generate Random number. may include negative numbers. max and min should be the exact number wanted.
     * e.g: wanted number ranging from 3,4,5,6 use max=6 and min=3 {genRandomNo(6,3)}
     *
     * @param max double
     * @param min double
     * @return double
     */
    public double genRandomNo(double max, double min) {
        DecimalFormat f = new DecimalFormat("##.0");
        return Double.parseDouble(f.format(random.nextDouble() * (max - min) + min));
    }
}