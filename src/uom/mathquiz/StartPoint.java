package uom.mathquiz;

import uom.mathquiz.controller.GameController;
import uom.mathquiz.view.MainFrame;

import javax.swing.*;

/**
 * Created by CipherHat on 2/10/14.
 */
public class StartPoint {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame ex = new MainFrame();
                GameController controller = new GameController(ex);
                ex.addAllButtonListener(controller);
            }
        });
    }
}
