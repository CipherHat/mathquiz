package uom.mathquiz.model.gamestate;

import uom.mathquiz.controller.GameController;

/**
 * Created by CipherHat on 2/14/14.
 */
public class GameRunning implements GameState {

    @Override
    public void proceed(GameController context) {

        context.getCountdownTimer().stopCountdown();

        context.setGameState(new GameStart());
        context.processAnswer();
    }
}
