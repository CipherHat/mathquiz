package uom.mathquiz.model.gamestate;

import uom.mathquiz.controller.GameController;

/**
 * Created by CipherHat on 2/14/14.
 */
public interface GameState {
    public void proceed(GameController context);
}
