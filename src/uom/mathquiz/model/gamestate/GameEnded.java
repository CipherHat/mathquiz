package uom.mathquiz.model.gamestate;

import uom.mathquiz.controller.GameController;

/**
 * Created by CipherHat on 3/1/14.
 */
public class GameEnded implements GameState {
    @Override
    public void proceed(GameController context) {
        context.setGameState(new GameStart());
    }
}
