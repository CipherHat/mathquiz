package uom.mathquiz.model.gamestate;

import uom.mathquiz.controller.GameController;
import uom.mathquiz.exception.ExpressionException;
import uom.mathquiz.external.CountdownTimer;
import uom.mathquiz.model.expressionvisitor.ExpressionSolver;

import java.util.List;

/**
 * Created by CipherHat on 2/14/14.
 */
public class GameStart implements GameState {
    @Override
    public void proceed(GameController context) {
        ExpressionSolver solver = new ExpressionSolver();

        int numAllowed = context.getGameStatus().getNumberAllowed();
        int parameter = context.getGameStatus().getParameter();
        int operator = context.getGameStatus().getOperator();
        context.getGenerator().setExpressionAttributes(numAllowed, numAllowed - (numAllowed * 2), parameter, operator);

        Double result = null;
        while (result == null) {
            result = generateResult(context, solver);
        }

        context.generateAnswer(result);

        context.startQuiz();
        context.setCountdownTimer(new CountdownTimer(context.getGameStatus().getTimeAllowed()));
        context.getCountdownTimer().addObserver(context);
        context.getCountdownTimer().startCountdown();

        context.setGameState(new GameRunning());
    }

    /**
     * check whether the answer for expression generated is valid for the application. Answer that contains more
     * than 1 floating points will be considered as invalid
     *
     * @param result double
     * @return double
     */
    private Double isValidAnswer(Double result) {
        result = Math.round(result * 100.0) / 100.0;
        String comparedFloatPoint = ("" + result).split("\\.")[1];
        if (comparedFloatPoint.contains("E") || comparedFloatPoint.length() > 1)
            return null;
        long point = Long.parseLong(comparedFloatPoint);
        if (point > 10) {
            return null;
        }
        return result;
    }

    /**
     * to ensure that expression generated is valid and solvable. If there are any expression that could lead to
     * invalid expression like division by zero, it will help to handle it.,
     *
     * @param context GameController
     * @param solver  ExpressionSolver
     * @return double
     */
    private Double generateResult(GameController context, ExpressionSolver solver) {
        Double result;
        try {
            context.getGenerator().generate();
            List<String> expressionAsList = context.getGenerator().getExpressionList();
            result = solver.solveExpression(expressionAsList);
        } catch (ExpressionException e) {
            return null;
        }
        return isValidAnswer(result);
    }
}
