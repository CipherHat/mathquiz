package uom.mathquiz.model.answerstate;

import uom.mathquiz.model.game.GameStatus;
import uom.mathquiz.model.game.GameStatusCaretaker;

/**
 * Created by CipherHat on 3/2/14.
 */
public abstract class GameStrategy {

    private final int MAX_PARAMETER = 5;
    private final int MAX_OPERATOR = 3;
    private final int[] ALLOWED_NUMBER = {9, 99, 999};
    private final int EVALUATE_OPERATOR = 3;
    private final int EVALUATE_PARAMETER = 2;
    private final int[] LEVEL_ALLOWED_NUMBER = {5, 10, 15};

    /**
     * Template method to handle User and change necessary attributes
     *
     * @param status    GameStatus
     * @param caretaker GameStatusCaretaker
     */
    public void handleGameStatus(GameStatus status, GameStatusCaretaker caretaker) {
        handleMemento(status, caretaker);

        status.setRounds(status.getRounds() + 1);
        status.setLevel(handleLevel(status.getLevel()));

        if (isLevelStateChangeRequired()) {
            if (status.getLevel() % EVALUATE_OPERATOR == 0) {
                if (status.getOperator() < MAX_OPERATOR)
                    status.setOperator(handleOperator(status.getOperator()));
            } else if (status.getLevel() % EVALUATE_PARAMETER == 0) {
                if (status.getParameter() < MAX_PARAMETER)
                    status.setParameter(handleParameter(status.getParameter()));
            }

            for (int i = 0; i < ALLOWED_NUMBER.length; i++) {
                if (status.getLevel() == LEVEL_ALLOWED_NUMBER[i]) {
                    status.setNumberAllowed(ALLOWED_NUMBER[i]);
                    i = 100;
                }
            }
        }

        status.setTimeAllowed(handleTimeAllowed(status.getLevel(), status.getTimeAllowed()));
        status.setLoseCount(handleLoseCount(status.getLoseCount()));
        status.setWinCount(handleWinCount(status.getWinCount()));
        status.setWinningStreak(handleWinningStreak(status.getWinningStreak()));
        status.setHighestWinningStreak(handleHighestWinningStreak(status.getHighestWinningStreak(), status.getWinningStreak()));
    }

    /**
     * handler for saving or undo the memento
     *
     * @param status    GameStatus
     * @param caretaker GameStatusCaretaker
     */
    protected abstract void handleMemento(GameStatus status, GameStatusCaretaker caretaker);

    /**
     * handler for manipulating Highest Winning Streak
     *
     * @param highestWinningStreak int
     * @param winningStreak        int
     * @return int
     */
    protected abstract int handleHighestWinningStreak(int highestWinningStreak, int winningStreak);

    /**
     * handler for manipulating Winning Streak
     *
     * @param winningStreak int
     * @return int
     */
    protected abstract int handleWinningStreak(int winningStreak);

    /**
     * handler for manipulating Time Allowed
     *
     * @param level       int
     * @param timeAllowed int
     * @return int
     */
    protected abstract int handleTimeAllowed(int level, int timeAllowed);

    /**
     * handler for manipulating cumulative winning count
     *
     * @param winCount int
     * @return int
     */
    protected abstract int handleWinCount(int winCount);

    /**
     * handler for manipulating cumulative losing count
     *
     * @param loseCount int
     * @return int
     */
    protected abstract int handleLoseCount(int loseCount);

    /**
     * handler for determining whether LevelState change is required or not
     *
     * @return int
     */
    protected abstract boolean isLevelStateChangeRequired();

    /**
     * handler for manipulating game level
     *
     * @param level int
     * @return int
     */
    protected abstract int handleLevel(int level);

    /**
     * handler for manipulating expression parameter
     *
     * @param parameter int
     * @return int
     */
    protected abstract int handleParameter(int parameter);

    /**
     * handler for manipulating operator
     *
     * @param operator int
     * @return int
     */
    protected abstract int handleOperator(int operator);
}
