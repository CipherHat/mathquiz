package uom.mathquiz.model.answerstate;

/**
 * Created by CipherHat on 3/2/14.
 *
 * SINGLETON
 */
public class GameStrategyFactory {
    private static GameStrategyFactory instance = new GameStrategyFactory();

    private GameStrategyFactory() {
    }

    public static synchronized GameStrategyFactory getInstance() {
        if (instance == null) {
            instance = new GameStrategyFactory();
        }
        return instance;
    }

    /**
     * depending on the answer provided, if it is correct (boolean true), will return concrete strategy
     * to handle correct answer strategy.
     *
     * @param isCorrect boolean
     * @return GameStrategy
     */
    public GameStrategy getStrategy(boolean isCorrect) {
        if (isCorrect) {
            return new CorrectAnswer();
        } else {
            return new WrongAnswer();
        }
    }
}
