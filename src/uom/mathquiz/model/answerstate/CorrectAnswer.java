package uom.mathquiz.model.answerstate;

import uom.mathquiz.model.game.GameStatus;
import uom.mathquiz.model.game.GameStatusCaretaker;

/**
 * Created by CipherHat on 3/2/14.
 */
public class CorrectAnswer extends GameStrategy {
    @Override
    protected void handleMemento(GameStatus status, GameStatusCaretaker caretaker) {
        caretaker.save(status);
    }

    @Override
    protected int handleHighestWinningStreak(int highestWinningStreak, int winningStreak) {
        if (winningStreak > highestWinningStreak)
            return winningStreak;
        return highestWinningStreak;
    }

    @Override
    protected int handleWinningStreak(int winningStreak) {
        return winningStreak + 1;
    }

    @Override
    protected int handleTimeAllowed(int level, int timeAllowed) {
        if (level <= 16)
            return level + 10;
        return timeAllowed - 1;
    }

    @Override
    protected int handleWinCount(int winCount) {
        return winCount + 1;
    }

    @Override
    protected int handleLoseCount(int loseCount) {
        return loseCount;
    }

    @Override
    protected boolean isLevelStateChangeRequired() {
        return true;
    }

    @Override
    protected int handleLevel(int level) {
        return level + 1;
    }

    @Override
    protected int handleParameter(int parameter) {
        return parameter + 1;
    }

    @Override
    protected int handleOperator(int operator) {
        return operator + 1;
    }
}
