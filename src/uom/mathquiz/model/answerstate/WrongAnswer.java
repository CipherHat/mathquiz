package uom.mathquiz.model.answerstate;

import uom.mathquiz.model.game.GameStatus;
import uom.mathquiz.model.game.GameStatusCaretaker;

/**
 * Created by CipherHat on 3/2/14.
 */
public class WrongAnswer extends GameStrategy {
    @Override
    protected void handleMemento(GameStatus status, GameStatusCaretaker caretaker) {
        if (!caretaker.isFinalRound())
            caretaker.undo(status);
    }

    @Override
    protected int handleHighestWinningStreak(int highestWinningStreak, int winningStreak) {
        return highestWinningStreak;
    }

    @Override
    protected int handleWinningStreak(int winningStreak) {
        return 0;
    }

    @Override
    protected int handleTimeAllowed(int level, int timeAllowed) {
        if (level <= 16)
            return level + 10;
        return timeAllowed + 1;
    }

    @Override
    protected int handleWinCount(int winCount) {
        return winCount;
    }

    @Override
    protected int handleLoseCount(int loseCount) {
        return loseCount + 1;
    }

    @Override
    protected boolean isLevelStateChangeRequired() {
        return false;
    }

    @Override
    protected int handleLevel(int level) {
        return level;
    }

    @Override
    protected int handleParameter(int parameter) {
        return parameter;
    }

    @Override
    protected int handleOperator(int operator) {
        return operator;
    }
}
