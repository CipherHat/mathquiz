package uom.mathquiz.model.game;

import java.util.Observable;

/**
 * Created by CipherHat on 3/2/14.
 */
public class GameStatus extends Observable {
    private LevelState levelState;
    private int rounds;
    private int winCount;
    private int loseCount;
    private int winningStreak;
    private int highestWinningStreak;
    private int timeAllowed;

    public GameStatus() {
        levelState = new LevelState();
        initializeGame();
    }

    public void initializeGame() {
        setParameter(2);
        setOperator(0);
        setNumberAllowed(9);
        setLevel(1);

        setRounds(1);
        setWinCount(0);
        setLoseCount(0);
        setWinningStreak(0);
        setHighestWinningStreak(0);
        setTimeAllowed(11);
    }

    //************************************************************************//
    //******************************** START *********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    public int getTimeAllowed() {
        return timeAllowed;
    }

    public void setTimeAllowed(int timeAllowed) {
        this.timeAllowed = timeAllowed;
        setChanged();
        notifyObservers("" + timeAllowed);
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
        setChanged();
        notifyObservers("Round " + rounds);
    }

    public int getWinCount() {
        return winCount;
    }

    public LevelState getLevelState() {
        return levelState;
    }

    public void setLevelState(LevelState levelState) {
        this.levelState = levelState;
    }

    public int getHighestWinningStreak() {
        return highestWinningStreak;
    }

    public void setHighestWinningStreak(int highestWinningStreak) {
        this.highestWinningStreak = highestWinningStreak;
        setChanged();
        notifyObservers("Highest Streak " + highestWinningStreak);
    }

    public void setWinCount(int winCount) {
        this.winCount = winCount;
        setChanged();
        notifyObservers("Win Count " + winCount);
    }

    public int getLoseCount() {
        return loseCount;
    }

    public void setLoseCount(int loseCount) {
        this.loseCount = loseCount;
        setChanged();
        notifyObservers("Lose Count " + loseCount);
    }

    public int getWinningStreak() {
        return winningStreak;
    }

    public void setWinningStreak(int winningStreak) {
        this.winningStreak = winningStreak;
        setChanged();
        notifyObservers("Winning Streak "+winningStreak);
    }

    public int getParameter() {
        return getLevelState().getParameter();
    }

    public void setParameter(int parameter) {
        getLevelState().setParameter(parameter);
    }

    public int getNumberAllowed() {
        return getLevelState().getNumberAllowed();
    }

    public void setNumberAllowed(int numberAllowed) {
        getLevelState().setNumberAllowed(numberAllowed);
    }

    public int getOperator() {
        return getLevelState().getOperator();
    }

    public void setOperator(int operator) {
        getLevelState().setOperator(operator);
    }

    public int getLevel() {
        return getLevelState().getLevel();
    }

    public void setLevel(int level) {
        getLevelState().setLevel(level);
        setChanged();
        notifyObservers("Level " + getLevel());
    }

    //************************************************************************//
    //******************************** END ***********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    /**
     * Memento operation for saving the Level State
     *
     * @return LevelMemento
     */
    public LevelMemento save() {
        return new LevelMemento(this.levelState);
    }

    /**
     * Memento operation for undo the Level State
     *
     * @param obj Object (should be LevelMemento instance)
     */
    public void undoToLastSave(Object obj) {
        LevelMemento memento = (LevelMemento) obj;
        setLevelState(memento.levelS);
        System.out.println(memento.levelS.getLevel() + " level");
    }

    /**
     * Memento for LevelState
     */
    private class LevelMemento {
        private LevelState levelS;

        public LevelMemento(LevelState levelS) {
            this.levelS = new LevelState();
            this.levelS.setNumberAllowed(levelS.getNumberAllowed());
            this.levelS.setOperator(levelS.getOperator());
            this.levelS.setLevel(levelS.getLevel());
            this.levelS.setParameter(levelS.getParameter());
        }
    }
}
