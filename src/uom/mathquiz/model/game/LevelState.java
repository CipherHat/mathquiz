package uom.mathquiz.model.game;

/**
 * Created by CipherHat on 3/2/14.
 */
public class LevelState {
    private int parameter;
    private int numberAllowed;
    private int operator;
    private int level;

    //************************************************************************//
    //******************************** START *********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    public int getParameter() {
        return parameter;
    }

    public void setParameter(int parameter) {
        this.parameter = parameter;
    }

    public int getNumberAllowed() {
        return numberAllowed;
    }

    public void setNumberAllowed(int numberAllowed) {
        this.numberAllowed = numberAllowed;
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    //************************************************************************//
    //******************************** END ***********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//
}
