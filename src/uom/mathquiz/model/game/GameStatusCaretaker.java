package uom.mathquiz.model.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CipherHat on 3/2/14.
 */
public class GameStatusCaretaker {
    private List<Object> objectList;

    public GameStatusCaretaker() {
        objectList = new ArrayList<Object>();
    }

    /**
     * Saving operation for the memento inside GameStatus
     *
     * @param status GameStatus
     */
    public void save(GameStatus status) {
        this.objectList.add(status.save());
        while (objectList.size() > 3) {
            objectList.remove(0);
        }
    }

    /**
     * Undo operation for the memento inside GameStatus
     *
     * @param status GameStatus
     */
    public void undo(GameStatus status) {
        status.undoToLastSave(objectList.remove(objectList.size() - 1));
    }

    /**
     * Check whether it is final round for the game or not. Basically checking whether memento can be undo
     * more or not
     *
     * @return boolean
     */
    public boolean isFinalRound() {
        return objectList.size() == 0;
    }
}
