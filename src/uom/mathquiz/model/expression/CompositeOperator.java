package uom.mathquiz.model.expression;

import uom.mathquiz.model.enums.ArithmeticParameter;
import uom.mathquiz.model.expressionvisitor.ExpressionVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CipherHat on 2/12/14.
 */
public class CompositeOperator implements Expression {
    private List<Expression> expressionList = new ArrayList<Expression>();
    private ArithmeticParameter arithmeticParameter;

    CompositeOperator() {
    }

    CompositeOperator(ArithmeticParameter arithmeticParameter) {
        this.arithmeticParameter = arithmeticParameter;
    }

    public List<Expression> getExpressionList() {
        return expressionList;
    }

    /**
     * Add new expression to the list
     *
     * @param expression Expression
     */
    public void addExpression(Expression expression) {
        this.expressionList.add(expression);
    }

    /**
     * Remove the desired expression from the list
     *
     * @param expression Expression
     */
    public void removeExpression(Expression expression) {
        this.expressionList.remove(expression);
    }

    //************************************************************************//
    //******************************** START *********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    public ArithmeticParameter getArithmeticParameter() {
        return arithmeticParameter;
    }

    public void setArithmeticParameter(ArithmeticParameter arithmeticParameter) {
        this.arithmeticParameter = arithmeticParameter;
    }

    //************************************************************************//
    //******************************** END ***********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    @Override
    public String print() {
        String print = "";
        for (int i = 0; i < expressionList.size(); i++) {
            print += expressionList.get(i).print();
            if (i != expressionList.size() - 1) {
                print += " " + arithmeticParameter.getSymbol() + " ";
            }
        }
        return print;
    }

    @Override
    public void accept(ExpressionVisitor visitor) {
        visitor.visit(this);
    }
}
