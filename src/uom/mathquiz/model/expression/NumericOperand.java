package uom.mathquiz.model.expression;

import uom.mathquiz.model.expressionvisitor.ExpressionVisitor;

/**
 * Created by CipherHat on 2/12/14.
 */
public class NumericOperand implements Expression {
    private double number;

    NumericOperand() {
    }

    NumericOperand(double number) {
        this.number = number;
    }

    //************************************************************************//
    //******************************** START *********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    //************************************************************************//
    //******************************** END ***********************************//
    //************************* GETTERS AND SETTERS **************************//
    //************************************************************************//

    @Override
    public String print() {
        String numberToReturn = "" + number;
        if (((number % number) == 0 ) || number == 0)
            numberToReturn = "" + (int) number;
        if (number < 0)
            return "( " + numberToReturn + " )";
        return numberToReturn;
    }

    @Override
    public void accept(ExpressionVisitor visitor) {
        visitor.visit(this);
    }
}
