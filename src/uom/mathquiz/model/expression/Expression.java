package uom.mathquiz.model.expression;

import uom.mathquiz.model.expressionvisitor.ExpressionVisitor;

/**
 * Created by CipherHat on 2/12/14.
 */
public interface Expression {
    /**
     * return the contained expression in String
     *
     * @return String
     */
    public String print();

    /**
     * Visitable method
     *
     * @param visitor ExpressionVisitor
     */
    public void accept(ExpressionVisitor visitor);
}
