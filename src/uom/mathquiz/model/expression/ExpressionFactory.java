package uom.mathquiz.model.expression;

import uom.mathquiz.model.enums.ArithmeticParameter;

/**
 * Created by CipherHat on 2/12/14.
 *
 * SINGLETON
 */
public class ExpressionFactory {
    private static ExpressionFactory instance;

    private ExpressionFactory() {
    }

    public static synchronized ExpressionFactory getInstance() {
        if (instance == null) {
            instance = new ExpressionFactory();
        }
        return instance;
    }

    /**
     * Get Atomic Expression (number) by providing a number as parameter
     *
     * @param number double
     * @return Expression
     */
    public Expression getExpression(double number) {
        return new NumericOperand(number);
    }

    /**
     * get Composite Expression (operator) by providing ArithmeticParameter as parameter
     *
     * @param param ArithmeticParameter
     * @return Expression
     */
    public Expression getExpression(ArithmeticParameter param) {
        return new CompositeOperator(param);
    }

    /**
     * get the Expression by providing the ExpressionParameter
     *
     * @param expressionParameter ExpressionParameter
     * @return Expression
     */
    public Expression getExpression(ExpressionParameter expressionParameter) {
        switch (expressionParameter) {
            case ATOMIC:
                return new NumericOperand();
            case COMPOSITE:
                return new CompositeOperator();
        }
        return null;
    }

    public enum ExpressionParameter {
        ATOMIC, COMPOSITE
    }
}
