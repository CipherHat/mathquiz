package uom.mathquiz.model.enums;

import uom.mathquiz.exception.ExpressionException;

/**
 * Created by CipherHat on 2/10/14.
 */
public enum ArithmeticParameter {
    ADD('+', 1, 0) {
        @Override
        public void checkValidity(double num1, double num2) throws ExpressionException {
        }
    }, MINUS('-', 1, 1) {
        @Override
        public void checkValidity(double num1, double num2) throws ExpressionException {
        }
    }, MULTIPLY('*', 2, 2) {
        @Override
        public void checkValidity(double num1, double num2) throws ExpressionException {
        }
    }, DIVIDE('/', 2, 3) {
        @Override
        public void checkValidity(double num1, double num2) throws ExpressionException {
            if (num2 == 0)
                throw new ExpressionException(num1, num2, getSymbol());
        }
    }, OPEN_BRACKET('(', 0, 99) {
        @Override
        public void checkValidity(double num1, double num2) throws ExpressionException {
        }
    }, CLOSE_BRACKET(')', 3, 99) {
        @Override
        public void checkValidity(double num1, double num2) throws ExpressionException {
        }
    };

    private char symbol;
    private int priority;
    private int selection;

    private ArithmeticParameter(char symbol, int priority, int selection) {
        this.symbol = symbol;
        this.priority = priority;
        this.selection = selection;
    }

    //************************************************************************//
    //******************************** START *********************************//
    //******************************* GETTERS ********************************//
    //************************************************************************//

    public int getPriority() {
        return priority;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getSelection() {
        return selection;
    }

    //************************************************************************//
    //********************************* END **********************************//
    //******************************* GETTERS ********************************//
    //************************************************************************//

    /**
     * Get Arithmetic Parameter by Symbol in char. e.g: '+', '-', '*', '/'
     *
     * @param symbol char
     * @return ArithmeticParameter
     */
    public static ArithmeticParameter getArithmeticBySymbol(char symbol) {
        for (ArithmeticParameter parameter : ArithmeticParameter.values()) {
            if (parameter.getSymbol() == symbol)
                return parameter;
        }
        return null;
    }

    /**
     * Get Arithmetic Parameter by Selection in int.
     *
     * @param selection int
     * @return ArithmeticParameter
     */
    public static ArithmeticParameter getArithmeticBySelection(int selection) {
        for (ArithmeticParameter parameter : ArithmeticParameter.values()) {
            if (parameter.getSelection() == selection)
                return parameter;
        }
        return null;
    }

    public abstract void checkValidity(double num1, double num2) throws ExpressionException;
}