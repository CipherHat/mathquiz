package uom.mathquiz.model.expressionvisitor;

import uom.mathquiz.exception.ExpressionException;
import uom.mathquiz.model.enums.ArithmeticParameter;
import uom.mathquiz.model.expression.CompositeOperator;
import uom.mathquiz.model.expression.Expression;
import uom.mathquiz.model.expression.ExpressionFactory;
import uom.mathquiz.model.expression.NumericOperand;

import java.util.List;
import java.util.Stack;

import static uom.mathquiz.model.enums.ArithmeticParameter.CLOSE_BRACKET;
import static uom.mathquiz.model.enums.ArithmeticParameter.OPEN_BRACKET;

/**
 * Created by CipherHat on 2/11/14.
 */
public class ExpressionSolver implements ExpressionVisitor {

    Stack<Double> numberStack = new Stack<Double>();
    Stack<ArithmeticParameter> operatorStack = new Stack<ArithmeticParameter>();

    /**
     * deal with expression as a List
     *
     * @param expression List<String>
     * @return double
     * @throws uom.mathquiz.exception.ExpressionException
     */
    public double solveExpression(List<String> expression) throws ExpressionException {
        for (String s : expression) {
            if (isNumber(s))
                numberStack.push(Double.parseDouble(s));
            else {
                dealWithOperator(s.charAt(0));
            }
        }
        while (!operatorStack.empty()) {
            operation();
        }
        return numberStack.peek();
    }

    //    /**
//     * deal with expression as a String. simply write e.g: "3+5*(4-1)"
//     * @param expression
//     * @return
//     *
//     */
//    public double solveExpression(String expression) {
//        char c;
//        for (int i = 0; i < expression.length(); i++) {
//            c = expression.charAt(i);
//            if ((int) c != 32)
//                if ((int) c >= 48 && (int) c <= 57)
//                    numberStack.push((int) c - 48);
//                else {
//                    dealWithOperator(c);
//                }
//        }
//        while (!operatorStack.empty()) {
//            operation();
//        }
//        return numberStack.peek();
//    }

    private void operation() throws ExpressionException {
        ExpressionFactory factory = ExpressionFactory.getInstance();

        double num2 = numberStack.pop(), num1 = numberStack.pop();
        ArithmeticParameter p = operatorStack.pop();

        p.checkValidity(num1, num2);

        Expression arithmetic = factory.getExpression(p);
        ((CompositeOperator) arithmetic).addExpression(factory.getExpression(num1));
        ((CompositeOperator) arithmetic).addExpression(factory.getExpression(num2));

        arithmetic.accept(this);
    }

    private boolean isNumber(String s) {
        try {
            //noinspection ResultOfMethodCallIgnored
            Double.parseDouble(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    private void dealWithOperator(char c) throws ExpressionException {
        ArithmeticParameter arithmeticSymbol = ArithmeticParameter.getArithmeticBySymbol(c);
        if (arithmeticSymbol == OPEN_BRACKET)
            operatorStack.push(OPEN_BRACKET);
        else {
            if (operatorStack.empty() || arithmeticSymbol.getPriority() > operatorStack.peek().getPriority())
                if (arithmeticSymbol == CLOSE_BRACKET) {
                    while (operatorStack.peek() != OPEN_BRACKET) {
                        operation();
                    }
                    operatorStack.pop();
                } else
                    operatorStack.push(arithmeticSymbol);
            else {
                while (!operatorStack.empty() && arithmeticSymbol.getPriority() <= operatorStack.peek().getPriority()) {
                    operation();
                }
                operatorStack.push(arithmeticSymbol);
            }
        }
    }

    @Override
    public void visit(NumericOperand atomic) {

    }

    @Override
    public void visit(CompositeOperator composite) {
        NumericOperand x = (NumericOperand) composite.getExpressionList().get(0);
        NumericOperand y = (NumericOperand) composite.getExpressionList().get(1);

        switch (composite.getArithmeticParameter()) {
            case ADD:
                numberStack.push(x.getNumber() + y.getNumber());
                break;
            case MINUS:
                numberStack.push(x.getNumber() - y.getNumber());
                break;
            case MULTIPLY:
                numberStack.push(x.getNumber() * y.getNumber());
                break;
            case DIVIDE:
                numberStack.push(x.getNumber() / y.getNumber());
                break;
            default:
                break;
        }
    }
}
