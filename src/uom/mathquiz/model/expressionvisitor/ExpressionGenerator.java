package uom.mathquiz.model.expressionvisitor;

import uom.mathquiz.external.MathClass;
import uom.mathquiz.model.enums.ArithmeticParameter;
import uom.mathquiz.model.expression.CompositeOperator;
import uom.mathquiz.model.expression.Expression;
import uom.mathquiz.model.expression.ExpressionFactory;
import uom.mathquiz.model.expression.NumericOperand;

import java.util.Arrays;
import java.util.List;

/**
 * Created by CipherHat on 2/12/14.
 */
public class ExpressionGenerator implements ExpressionVisitor {
    //variables that may change influenced by the game model.
    private int maxNum;
    private int minNum;
    private int parameter;
    private int operator;

    private int paramCount;

    private String expression;
    private ExpressionFactory expressionFactory;
    private MathClass mathClass;

    public ExpressionGenerator() {
        this.expressionFactory = ExpressionFactory.getInstance();
        this.mathClass = MathClass.getInstance();
        this.maxNum = 9;
        this.minNum = -9;
        this.parameter = 2;
        this.operator = 0;
        this.paramCount = 0;
    }

    /**
     * The single point to generate expression.
     */
    public void generate() {
        Expression composite = expressionFactory.getExpression(ExpressionFactory.ExpressionParameter.COMPOSITE);

        paramCount = 0; // required to control the parameter generated
        composite.accept(this);

        expression = composite.print();
    }

    /**
     * The single point to get expression in String.
     *
     * @return String
     */
    public String getExpressionString() {
        if (expression == null)
            generate();

        return expression;
    }

    /**
     * The single point to get expression in List.
     *
     * @return List<String>
     */
    public List<String> getExpressionList() {
        return Arrays.asList(getExpressionString().split(" "));
    }

    /**
     * the single point to manipulate the expression dynamically.
     *
     * @param maxNum    int
     * @param minNum    int
     * @param parameter int
     * @param operator  int (refer to ArithmeticParameter to know the range. currently 0-3 (size:4)
     */
    public void setExpressionAttributes(int maxNum, int minNum, int parameter, int operator) {
        this.maxNum = maxNum;
        this.minNum = minNum;
        this.parameter = parameter;
        this.operator = operator;
    }

    @Override
    public void visit(NumericOperand atomic) {
        atomic.setNumber(mathClass.genRandomNo(maxNum, minNum));
    }

    @Override
    public void visit(CompositeOperator composite) {
        composite.setArithmeticParameter(ArithmeticParameter.getArithmeticBySelection(mathClass.genRandomNo(operator, 0)));

        int atLeastOneParam = 0;
        while (paramCount < parameter) {
            Expression e;
            int randomExp = 1; // by default, will generate number unless already have 1 number per operator
            if (atLeastOneParam >= 1)
                randomExp = mathClass.genRandomNo(1, 0);

            switch (randomExp) {default:
                case 0://composite
                    e = expressionFactory.getExpression(ExpressionFactory.ExpressionParameter.COMPOSITE);
                    break;
                case 1://atomic
                    e = expressionFactory.getExpression(ExpressionFactory.ExpressionParameter.ATOMIC);
                    paramCount++;
                    atLeastOneParam++;
                    break;
            }
            e.accept(this);
            composite.addExpression(e);
        }
    }
}
