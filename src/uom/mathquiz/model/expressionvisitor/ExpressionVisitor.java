package uom.mathquiz.model.expressionvisitor;

import uom.mathquiz.model.expression.CompositeOperator;
import uom.mathquiz.model.expression.NumericOperand;

/**
 * Created by CipherHat on 2/27/14.
 */
public interface ExpressionVisitor {
    /**
     * visit atomic Expression
     *
     * @param atomic NumericOperand
     */
    public void visit(NumericOperand atomic);

    /**
     * visit composite Expression
     *
     * @param composite CompositeOperator
     */
    public void visit(CompositeOperator composite);
}
